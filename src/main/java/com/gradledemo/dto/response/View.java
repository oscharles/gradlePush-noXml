package com.gradledemo.dto.response;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2015-01-23.
 */
public class View {
    public interface Summary{};
    public interface SummaryWithRecipients extends Summary {}
}
