package com.gradledemo.dao;

import com.gradledemo.common.hibernate.HibernateDao;
import com.gradledemo.entity.AdminAuth;
import org.springframework.stereotype.Repository;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2015-01-29.
 */
@Repository
public class AdminAuthDao extends HibernateDao<AdminAuth, Integer> {
}
