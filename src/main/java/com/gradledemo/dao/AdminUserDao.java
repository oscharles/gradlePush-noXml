package com.gradledemo.dao;

import com.gradledemo.common.hibernate.HibernateDao;
import com.gradledemo.entity.AdminUser;
import org.springframework.stereotype.Repository;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2014-08-08.
 */
@Repository
public class AdminUserDao extends HibernateDao<AdminUser,Integer>{
}
