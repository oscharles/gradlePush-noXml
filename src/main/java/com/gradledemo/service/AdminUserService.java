package com.gradledemo.service;

import com.gradledemo.dao.AdminUserDao;
import com.gradledemo.entity.AdminUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2014-08-08.
 */
@Service
public class AdminUserService {
    @Autowired
    public AdminUserDao adminUserDao;

    /**
     * 获得所有管理员
     *
     * @return 返回管理员集合
     */
    @Transactional(readOnly = true)
    @Cacheable(value = "adminUser")
    public List<AdminUser> getAll() {
        return adminUserDao.getAll();
    }

    /**
     * 根据管理员 Id 获得管理员信息
     *
     * @param adminId 管理员 Id
     * @return 管理员信息
     */
    @Transactional(readOnly = true)
    @Cacheable(value = "adminUser", key = "#adminId")
    public AdminUser getId(Integer adminId) {
        return adminUserDao.get(adminId);
    }

    /**
     * 根据管理员登录名获得管理员信息
     *
     * @param adminName 管理员登录名
     * @return 管理员信息
     */
    @Transactional(readOnly = true)
    @Cacheable(value = "adminUser", key = "#adminName")
    public AdminUser getInfoByName(String adminName) {
        return adminUserDao.findUniqueBy("adminName", adminName);
    }

}
