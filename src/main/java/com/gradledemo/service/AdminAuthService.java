package com.gradledemo.service;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

import com.gradledemo.dao.AdminAuthDao;
import com.gradledemo.entity.AdminAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Charles.Tan on 2015-01-29.
 */
@Service
public class AdminAuthService {
    @Autowired
    private AdminAuthDao adminAuthDao;

    /**
     * 根据管理员 Id，获得所有管理员权限。
     * @param adminId 管理员 Id
     * @return 管理员权限集合。
     */
    @Transactional(readOnly = true)
    @Cacheable(value = "adminAuth",key = "#adminId")
    public List<AdminAuth> getAdminAuthByAdminId(int adminId) {
        return adminAuthDao.findBy("adminId", adminId);
    }
}
