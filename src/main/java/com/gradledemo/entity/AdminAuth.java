package com.gradledemo.entity;


import javax.persistence.*;
import java.io.Serializable;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2014-08-08.
 */
@Entity
@Table(name = "admin_auth", schema = "")
public class AdminAuth implements Serializable{
    private int authId;
    private String authVal;
    private Integer adminId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "auth_id")
    public int getAuthId() {
        return authId;
    }

    public void setAuthId(int authId) {
        this.authId = authId;
    }

    @Basic
    @Column(name = "auth_val")
    public String getAuthVal() {
        return authVal;
    }

    public void setAuthVal(String authVal) {
        this.authVal = authVal;
    }

    @Basic
    @Column(name = "admin_id")
    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminAuth adminAuth = (AdminAuth) o;

        if (authId != adminAuth.authId) return false;
        if (adminId != null ? !adminId.equals(adminAuth.adminId) : adminAuth.adminId != null) return false;
        if (authVal != null ? !authVal.equals(adminAuth.authVal) : adminAuth.authVal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = authId;
        result = 31 * result + (authVal != null ? authVal.hashCode() : 0);
        result = 31 * result + (adminId != null ? adminId.hashCode() : 0);
        return result;
    }
}
