package com.gradledemo.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2014-08-08.
 */
@Entity
@Table(name = "admin_user", schema = "")
public class AdminUser implements Serializable {
    private int adminId;
    private String adminName;
    private String psw;
    private String verificationCode;
    private Timestamp verificationDatetime;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_id")
    public int getAdminId() {
        return adminId;
    }

    public void setAdminId(int adminId) {
        this.adminId = adminId;
    }

    @Basic
    @Column(name = "admin_name")
    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    @Basic
    @Column(name = "psw")
    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    @Basic
    @Column(name = "verification_Code")
    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    @Basic
    @Column(name = "verification_datetime")
    public Timestamp getVerificationDatetime() {
        return verificationDatetime;
    }

    public void setVerificationDatetime(Timestamp verificationDatetime) {
        this.verificationDatetime = verificationDatetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdminUser adminUser = (AdminUser) o;

        if (adminId != adminUser.adminId) return false;
        if (adminName != null ? !adminName.equals(adminUser.adminName) : adminUser.adminName != null) return false;
        if (psw != null ? !psw.equals(adminUser.psw) : adminUser.psw != null) return false;
        if (verificationCode != null ? !verificationCode.equals(adminUser.verificationCode) : adminUser.verificationCode != null)
            return false;
        if (verificationDatetime != null ? !verificationDatetime.equals(adminUser.verificationDatetime) : adminUser.verificationDatetime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = adminId;
        result = 31 * result + (adminName != null ? adminName.hashCode() : 0);
        result = 31 * result + (psw != null ? psw.hashCode() : 0);
        result = 31 * result + (verificationCode != null ? verificationCode.hashCode() : 0);
        result = 31 * result + (verificationDatetime != null ? verificationDatetime.hashCode() : 0);
        return result;
    }
}
