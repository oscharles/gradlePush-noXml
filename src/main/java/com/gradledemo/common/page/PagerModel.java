package com.gradledemo.common.page;

import java.util.ArrayList;
import java.util.List;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Administrator on 2014/4/16.
 * 分页模型
 */
public class PagerModel<T> {
	/**
	 * 总记录数
	 */
	private Long total;
	/**
	 * 当前页面数据列表
	 */
	private List<T> rows;

    /**
     * 每页多少条
     */
    private int pageSize;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public List<T> getRows() {
		return null == rows ? new ArrayList<T>() : rows;
	}
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
}