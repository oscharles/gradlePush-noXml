package com.gradledemo.common;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Administrator on 13-12-3.
 */
public class Constant {

    /**
     * 国际化集合数据
     */
    public static final Map<String,Locale> localeMap=new HashMap<String,Locale>(){
        {
            put("zh",new Locale("zh", "CN"));
            put("en",new Locale("en", "US"));
        }
    };
}
