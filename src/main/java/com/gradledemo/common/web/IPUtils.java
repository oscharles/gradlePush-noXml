package com.gradledemo.common.web;


import javax.servlet.http.HttpServletRequest;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Administrator on 13-12-7.
 */
public class IPUtils {

    /**
     * 获得客户端的真实IP
     * @param request Http 请求
     * @return String
     */
    public static final String getIPAddr(final HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        final String arr[] = ip.split(",");
        for (final String s : arr) {
            if (!"unknown".equalsIgnoreCase(s)) {
                ip = s;
                break;
            }
        }
        return ip;
    }
}
