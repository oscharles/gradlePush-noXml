package com.gradledemo.common.CustomException;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles on 14-1-24.
 */
public class ParamsException extends Exception{
    public ParamsException() {
    }

    public ParamsException(String message) {
        super(message);
    }
}
