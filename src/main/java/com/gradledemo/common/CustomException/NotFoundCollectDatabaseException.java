package com.gradledemo.common.CustomException;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles on 14-1-2.
 */
public class NotFoundCollectDatabaseException extends Exception{
    public NotFoundCollectDatabaseException() {
    }

    public NotFoundCollectDatabaseException(String message) {
        super(message);
    }
}
