package com.gradledemo.common.CustomException;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Administrator on 13-12-4.
 */
public class SPIDNotAccessException extends Exception{
    public SPIDNotAccessException() {
    }

    public SPIDNotAccessException(String message) {
        super(message);
    }
}
