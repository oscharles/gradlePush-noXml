package com.gradledemo.common.CustomException;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles on 13-12-17.
 */
public class PageSizeIsNullException extends Exception{
    public PageSizeIsNullException() {
    }

    public PageSizeIsNullException(String message) {
        super(message);
    }
}
