package com.gradledemo.common.CustomException;

import java.io.IOException;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by charles on 13-12-4.
 */
public class FileSizeIsLargeException extends IOException {

    public FileSizeIsLargeException() {
    }

    public FileSizeIsLargeException(String message) {
        super(message);
    }
}
