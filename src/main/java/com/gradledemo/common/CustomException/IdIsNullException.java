package com.gradledemo.common.CustomException;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles on 14-1-2.
 */
public class IdIsNullException extends Exception{
    public IdIsNullException() {
    }

    public IdIsNullException(String message) {
        super(message);
    }
}
