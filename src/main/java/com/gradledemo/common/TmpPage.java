package com.gradledemo.common;

import com.google.common.collect.Lists;

import java.util.List;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Administrator on 13-12-16.
 */
public class TmpPage<T> {
    private List<T> list = Lists.newArrayList();
    private long totalCount = 0l;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}
