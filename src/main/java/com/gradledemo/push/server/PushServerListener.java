package com.gradledemo.push.server;

import org.comet4j.core.CometContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Charles.Tan on 2014-08-08.
 */
public class PushServerListener implements ServletContextListener {
    private static final String CHANNEL = "hello";
    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        CometContext cc = CometContext.getInstance();
        cc.registChannel(CHANNEL);//注册应用的channel
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
