package com.gradledemo.controller.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.gradledemo.dto.response.Message;
import com.gradledemo.dto.response.View;
import com.gradledemo.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

@RestController
@RequestMapping("/m")
public class MessageAction {

	@Autowired
	private MessageService messageService;

	@JsonView(View.Summary.class)
	@RequestMapping("/")
	public List<Message> getAllMessages() {
		return messageService.getAll();
	}

	@JsonView(View.SummaryWithRecipients.class)
	@RequestMapping("/with-recipients")
	public List<Message> getAllMessagesWithRecipients() {
		return messageService.getAll();
	}

	@JsonView(View.Summary.class)
	@RequestMapping("/{id}")
	public Message getMessage(@PathVariable Long id) {
		return this.messageService.get(id);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Message create(@RequestBody Message message) {
		return this.messageService.create(message);
	}

}
