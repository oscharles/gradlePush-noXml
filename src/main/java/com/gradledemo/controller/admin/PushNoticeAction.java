package com.gradledemo.controller.admin;

import com.fasterxml.jackson.annotation.JsonView;
import com.gradledemo.dto.response.Message;
import com.gradledemo.dto.response.User;
import com.gradledemo.dto.response.View;
import com.gradledemo.service.MessageService;
import org.apache.http.entity.ContentType;
import org.comet4j.core.CometContext;
import org.comet4j.core.CometEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Lenovo2 on 2014-08-08.
 */
@Controller
public class PushNoticeAction extends BaseAdminAction {
    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "push", produces = {MediaType.APPLICATION_XML_VALUE})
    @ResponseBody
    public Map<String, String> push(@RequestParam("num") String num) {
        CometEngine engine = CometContext.getInstance().getEngine();
        engine.sendToAll("hello", num);
        Map<String, String> map = new HashMap<>();
        new User();
        return map;
    }

    @JsonView(View.Summary.class)
    @RequestMapping(value = "msg")
    @ResponseBody
    public List<Message> getAllMessages() {
        return messageService.getAll();
    }
}
