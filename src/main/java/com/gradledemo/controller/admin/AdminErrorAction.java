package com.gradledemo.controller.admin;

import com.gradledemo.service.AdminUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Lenovo2 on 2014-08-08.
 */
@Controller
public class AdminErrorAction extends BaseAdminAction {

    @RequestMapping(value = "error")
    public String index(Model model) {
        return "admin/error/error";
    }
}
