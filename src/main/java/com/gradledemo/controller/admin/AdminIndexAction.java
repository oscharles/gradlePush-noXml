package com.gradledemo.controller.admin;

import com.gradledemo.service.AdminUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Lenovo2 on 2014-08-08.
 */
@Controller
public class AdminIndexAction extends BaseAdminAction {
    @Autowired
    public AdminUserService adminUserService;

    @RequiresPermissions("login:login")
    @RequestMapping(value = {"index"})
    public String index(Model model) {
        return "admin/index/index";
    }

    @RequestMapping(value = {"/", ""})
    public String noHtm(Model model) {
        return "redirect:/admin/index.htm";
    }

    @RequiresPermissions("login:login")
    @RequestMapping(value = {"home"})
    public String home(Model model) {
        return "admin/index/home";
    }
}
