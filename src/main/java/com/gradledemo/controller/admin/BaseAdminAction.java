package com.gradledemo.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * Copyright (c) 2010-2015 LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

/**
 * Created by Lenovo2 on 2014-08-08.
 */
@Controller
@RequestMapping("admin")
public class BaseAdminAction {
}
