/*
 * Copyright (c) 2010-2015. LiZaoNet Inc. (http://www.lizaonet.com).
 * license: http://www.lizaonet.com
 */

package com.spring;

import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityLayoutViewResolver;

import java.util.Properties;

/**
 * Created by charles on 15-1-24.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.gradledemo")
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    @Qualifier("securityManager")
    private DefaultWebSecurityManager securityManager;

    @Override
    public void configureDefaultServletHandling(
            DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/js/**").addResourceLocations("/js/");
        registry.addResourceHandler("/css/**").addResourceLocations("/css/");
        registry.addResourceHandler("/plugins/**").addResourceLocations("/plugins/");
        registry.addResourceHandler("/upload/**").addResourceLocations("/upload/");
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
        commonsMultipartResolver.setDefaultEncoding("UTF-8");
        commonsMultipartResolver.setMaxUploadSize(1024000000000L);
        return commonsMultipartResolver;
    }

    @Bean
    public VelocityConfigurer velocityConfigurer() {
        VelocityConfigurer velocityConfigurer = new VelocityConfigurer();
        velocityConfigurer.setResourceLoaderPath("/views/");
        Properties p = new Properties();
        p.put("input.encoding", "UTF-8");
        p.put("output.encoding", "UTF-8");
        velocityConfigurer.setVelocityProperties(p);
        return velocityConfigurer;
    }

    @Bean
    public VelocityLayoutViewResolver velocityViewResolver() {
        VelocityLayoutViewResolver velocityLayoutViewResolver = new VelocityLayoutViewResolver();
        velocityLayoutViewResolver.setLayoutUrl("layout/default.vm");
        velocityLayoutViewResolver.setSuffix(".vm");
        velocityLayoutViewResolver.setToolboxConfigLocation("/WEB-INF/classes/velocityToolBox.xml");
        velocityLayoutViewResolver.setContentType("text/html;charset=utf-8");
        velocityLayoutViewResolver.setViewClass(com.gradledemo.common.shiro.VelocityToolboxView20.class);
        velocityLayoutViewResolver.setAllowRequestOverride(true);
        velocityLayoutViewResolver.setAllowSessionOverride(true);
        return velocityLayoutViewResolver;
    }

    @Bean
    @DependsOn(value = "lifecycleBeanPostProcessor")
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        defaultAdvisorAutoProxyCreator.setProxyTargetClass(true);
        return defaultAdvisorAutoProxyCreator;
    }

    @Bean
    public AuthorizationAttributeSourceAdvisor advisor() {
        AuthorizationAttributeSourceAdvisor advisor = new AuthorizationAttributeSourceAdvisor();
        advisor.setSecurityManager(securityManager);
        return advisor;
    }

    @Bean
    public SimpleMappingExceptionResolver resolver() {
        SimpleMappingExceptionResolver resolver = new SimpleMappingExceptionResolver();
        Properties p = new Properties();
        p.put("org.apache.shiro.authz.UnauthorizedException", "error.htm");
        resolver.setExceptionMappings(p);
        return resolver;
    }

}
