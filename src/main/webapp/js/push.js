var JS, listener;

listener = window.listener;

JS = window.JS;

$(function() {
  'use strict';
  JS.Engine.on({
    hello: function(num) {
      return window.console.info(num);
    }
  });
  return JS.Engine.start(listener);
});
